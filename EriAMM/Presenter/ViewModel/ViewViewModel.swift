//
//  ViewViewModel.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 12/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation
import UIKit

struct ViewViewModel {
    var foto = UIImage()
    var descricao: String?
    var id: String?
    var nome: String?
    var link: String?
    var numero_principal: Int?
    var numero_secundario: Int?
    var tipo: Bool?
    var ultima_alteracao: Date?
        
    init(object: AMMModelInfrastructure) {
        self.foto = self.foto.dataToImage(data: object.foto!)
        self.descricao = object.descricao
        self.id = object.id
        self.nome = object.nome
        self.link = object.link
        self.numero_principal = object.numero_principal
        self.numero_secundario = object.numero_secundario
        self.tipo = object.tipo
        self.ultima_alteracao = object.ultima_alteracao
    }
}
