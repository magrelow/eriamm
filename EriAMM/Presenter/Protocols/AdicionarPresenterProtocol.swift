//
//  AdicionarPresenterProtocol.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 08/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation
import UIKit

protocol AdicionarPresenterProtocol {
    func adicionarTitulo(image: UIImage, titulo: String, aniMan: Bool, temMan: String, capEpi: String, desc: String, link: String, completionHandler: @escaping (PresenterError?) -> Void)
}
