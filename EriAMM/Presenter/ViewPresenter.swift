//
//  ViewPresenter.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 11/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

protocol ViewPresenterProtocol {
    func buscarDadosLista(completionHandler: @escaping ([ViewViewModel], PresenterError) -> Void)
}


class ViewPresenter: ViewPresenterProtocol {
    
    var buscarInfrastructure: BuscarInfrastructureProtocol = BuscarInfrastructure()
    
    func buscarDadosLista(completionHandler: @escaping ([ViewViewModel], PresenterError) -> Void) {
        buscarInfrastructure.buscarTudoAMMCoreData()
        { result, error in
            if error == .noError {
                var viewViewModel: [ViewViewModel] = []
                
                for model in result {
                    viewViewModel.append(ViewViewModel(object: model))
                }
                
                completionHandler(viewViewModel, .noError)
                
            } else {
                completionHandler([], .genericError)
                
            }
        }
    }
}
