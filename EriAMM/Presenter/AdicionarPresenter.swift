//
//  AdicionarPresenter.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 06/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation
import UIKit



class AdicionarPresenter: AdicionarPresenterProtocol {
    
    var adicionarInfrastructure: AdicionarInfrastructureProtocol = AdicionarInfrastructure()
    
    func adicionarTitulo(image: UIImage, titulo: String, aniMan: Bool, temMan: String, capEpi: String, desc: String, link: String, completionHandler: @escaping (PresenterError?) -> Void) {
        
        adicionarInfrastructure.adicionarValoresAoCoreData(image: image.saveToData(),
                                                           titulo: titulo,
                                                           aniMan: aniMan,
                                                           temMan: temMan,
                                                           capEpi: capEpi,
                                                           desc: desc,
                                                           link: link)
        { error in
            
            completionHandler(.noError)
            
        }
    }
}
