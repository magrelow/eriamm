//
//  ExtensionPresenter.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 08/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func saveToData() -> Data{
        return self.pngData()!
    }
    
    func dataToImage(data: Data) -> UIImage {
        return UIImage(data: data)!
    }
}
