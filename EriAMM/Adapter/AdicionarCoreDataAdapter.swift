//
//  AdicionarCoreDataAdapter.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 08/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation
import CoreData
import UIKit

protocol AdicionarCoreDataAdapterProtocol {
    func adicionarDadosCoreData (key: String,
                                 dados: [String: Any],
                                 completionHandler: @escaping(NSManagedObject?, AdapterErro) -> Void)
}

class AdicionarCoreDataAdapter: AdicionarCoreDataAdapterProtocol {

    func adicionarDadosCoreData (key: String,
                                 dados: [String: Any],
                                 completionHandler: @escaping(NSManagedObject?, AdapterErro) -> Void)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: key, in: context)
        let novoDado = NSManagedObject(entity: entity!, insertInto: context)
        
        for dado in dados {
            novoDado.setValue(dado.value, forKey: dado.key)
        }
        
        do {
            try context.save()
            completionHandler(novoDado, .noError)
        } catch {
            completionHandler(novoDado, .genericError)
        }
    }
    
}
