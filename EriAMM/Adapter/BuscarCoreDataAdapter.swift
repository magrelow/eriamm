//
//  BuscarCoreDataAdapter.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 10/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation
import CoreData
import UIKit

protocol BuscarCoreDataAdapterProtocol {
    func buscarTodosOsDados(key: String, completionHandler: @escaping([NSManagedObject]?, AdapterErro) -> Void)
}

class BuscarCoreDataAdapter: BuscarCoreDataAdapterProtocol {
    
    func buscarTodosOsDados(key: String,
                            completionHandler: @escaping([NSManagedObject]?, AdapterErro) -> Void) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: key)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            completionHandler((result as! [NSManagedObject]), .noError)
            //return (result as! [NSManagedObject])
        } catch {
            print("Failed")
            completionHandler(nil, .genericError)
        }
        
    }
}
