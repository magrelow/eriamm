//
//  AdapterErro.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 08/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

public enum AdapterErro {
    case genericError
    case imageNilError
    case imageDownloadError
    case noError
}
