import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    //Presenter
    var viewPresenter: ViewPresenterProtocol = ViewPresenter()
    
    //ViewModel
    var listaDeViewModel: [ViewViewModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initValuesForTableView()
    }
    
    func initValuesForTableView() {
        viewPresenter.buscarDadosLista() {
            [weak self] result, error in
            DispatchQueue.main.async {
                self?.listaDeViewModel = result
                self?.tableview.reloadData()
            }
            
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! MainContentTableViewCell
        cell.mangaCoverImage.image = listaDeViewModel[indexPath.row].foto
        cell.tituloLabel.text = listaDeViewModel[indexPath.row].nome
        cell.temporadaMangaLabel.text = "\(listaDeViewModel[indexPath.row].numero_principal!)"
        cell.capituloEpisodioLabel.text = "\(listaDeViewModel[indexPath.row].numero_secundario!)"
        cell.dataAlteracaoLabel.text = "\(listaDeViewModel[indexPath.row].ultima_alteracao!)"
        
        return cell
    }
}
