//
//  MainContentTableViewCell.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 03/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import UIKit

class MainContentTableViewCell: UITableViewCell {
    @IBOutlet weak var mangaCoverImage: UIImageView!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var temporadaMangaLabel: UILabel!
    @IBOutlet weak var capituloEpisodioLabel: UILabel!
    @IBOutlet weak var dataAlteracaoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
