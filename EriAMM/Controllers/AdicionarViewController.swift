import UIKit

class AdicionarViewController: UIViewController {

    @IBOutlet weak var tituloTextField_1: UITextField!
    @IBOutlet weak var tempMangaTextField_2: UITextField!
    @IBOutlet weak var capEpiTextField_3: UITextField!
    @IBOutlet weak var descTextField_4: UITextField!
    @IBOutlet weak var linkTextField_5: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var aniManSwitch: UISwitch!
    
    // to store the current active textfield
    var activeTextField : UITextField? = nil
    var imagePicker = UIImagePickerController()
    
    //Presenter
    var adicionarPresenter: AdicionarPresenterProtocol = AdicionarPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tituloTextField_1.delegate = self
        tempMangaTextField_2.delegate = self
        capEpiTextField_3.delegate = self
        descTextField_4.delegate = self
        linkTextField_5.delegate = self
        
        tituloTextField_1.tag = 1
        tempMangaTextField_2.tag = 2
        capEpiTextField_3.tag = 3
        descTextField_4.tag = 4
        linkTextField_5.tag = 5
        
        self.initializeHideKeyboard()
        self.setupToolbar()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @IBAction func btnClick(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")

            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false

            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func adicionarBtnClick(_ sender: UIButton) {
    
        if checkIfIsAllOK() {
            adicionarPresenter.adicionarTitulo(image: imageView.image!,
                                               titulo: tituloTextField_1.text!,
                                               aniMan: aniManSwitch.isOn,
                                               temMan: tempMangaTextField_2.text!,
                                               capEpi: capEpiTextField_3.text!,
                                               desc: descTextField_4.text!,
                                               link: linkTextField_5.text!)
            { [weak self] error in
                if error == .noError {
                    print("testes")
                    self?.navigationController?.popViewController(animated: false)
                }
            }
        }
    }
    
    func checkIfIsAllOK() -> Bool {
        if imageView.image == nil {
            alertError(title: "Erro", message: "Por favor, selecione uma imagem")
            return false
        } else if tituloTextField_1.text == "" {
            alertError(title: "Erro", message: "Por favor, coloque um titulo")
            return false
        } else if tempMangaTextField_2.text == "" {
            alertError(title: "Erro", message: "Por favor, coloque uma temporada ou manga")
            return false
        } else if capEpiTextField_3.text == "" {
            alertError(title: "Erro", message: "Por favor, coloque uma capitulo ou episodio")
            return false
        } else if descTextField_4.text == "" {
            alertError(title: "Erro", message: "Por favor, coloque uma descrição")
            return false
        } else {
            return true
        }
    }
    
    func alertError(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

        self.present(alert, animated: true)
    }
}

extension AdicionarViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
   
    func imagePickerController (_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        self.dismiss(animated: true, completion: { () -> Void in
            if let img = info[.originalImage] {
                self.imageView.image = (img as! UIImage)
            }
        })
    }
}

extension AdicionarViewController {
    
    func initializeHideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissMyKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissMyKeyboard(){
        view.endEditing(true)
    }
    
    func setupToolbar(){
        let bar = UIToolbar()
        let doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissMyKeyboard))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        bar.items = [flexSpace, flexSpace, doneBtn]
        bar.sizeToFit()
        
        tituloTextField_1.inputAccessoryView = bar
        tempMangaTextField_2.inputAccessoryView = bar
        capEpiTextField_3.inputAccessoryView = bar
        descTextField_4.inputAccessoryView = bar
        linkTextField_5.inputAccessoryView = bar
        
    }
}

extension AdicionarViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = self.view.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
      self.activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
      self.activeTextField = nil
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {

      guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
        return
      }

      var shouldMoveViewUp = false

      if let activeTextField = activeTextField {

        let bottomOfTextField = activeTextField.convert(activeTextField.bounds, to: self.view).maxY;
        
        let topOfKeyboard = self.view.frame.height - keyboardSize.height
        
        if bottomOfTextField > topOfKeyboard {
          shouldMoveViewUp = true
        }
      }

      if(shouldMoveViewUp) {
        self.view.frame.origin.y = 0 - keyboardSize.height
      }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
      self.view.frame.origin.y = 0
    }
}
