//
//  InfraError.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 08/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

enum InfraError {
    case genericError
    case noError
}
