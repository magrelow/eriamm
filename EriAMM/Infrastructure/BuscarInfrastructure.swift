//
//  BuscarInfrastructure.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 11/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

protocol BuscarInfrastructureProtocol {
    func buscarTudoAMMCoreData(completionHandler: @escaping([AMMModelInfrastructure], InfraError) -> Void)
}

class BuscarInfrastructure: BuscarInfrastructureProtocol {
    
    var buscarCoreDataAdapter: BuscarCoreDataAdapterProtocol = BuscarCoreDataAdapter()
    
    func buscarTudoAMMCoreData(completionHandler: @escaping([AMMModelInfrastructure], InfraError) -> Void) {
        buscarCoreDataAdapter.buscarTodosOsDados(key: ConstanteInfrastructure().keyAmm)
        { result, error in
            
            if error == .noError {
                var ammModelInfrastructure: [AMMModelInfrastructure] = []
                
                result?.forEach {
                    ammModelInfrastructure.append(AMMModelInfrastructure(object: $0))
                }
                
                completionHandler(ammModelInfrastructure, .noError)
            } else {
                completionHandler([], .genericError)
            }
        }
    }
}
