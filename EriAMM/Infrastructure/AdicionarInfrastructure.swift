//
//  AdicionarInfrastructure.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 08/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation



class AdicionarInfrastructure: AdicionarInfrastructureProtocol {
    
    var adicionarCoreData: AdicionarCoreDataAdapterProtocol = AdicionarCoreDataAdapter()
    
    func adicionarValoresAoCoreData(image: Data, titulo: String, aniMan: Bool, temMan: String, capEpi: String, desc: String, link: String, completionHandler: @escaping(InfraError) -> Void) {
        
        var dictionary = [String: Any]()
        dictionary.updateValue(image, forKey: "foto")
        dictionary.updateValue(desc, forKey: "descricao")
        dictionary.updateValue(UUID().description, forKey: "id")
        dictionary.updateValue(titulo, forKey: "nome")
        dictionary.updateValue(link, forKey: "link")
        dictionary.updateValue(Int(temMan) as Any, forKey: "numero_principal")
        dictionary.updateValue(Int(capEpi) as Any, forKey: "numero_secundario")
        dictionary.updateValue(aniMan, forKey: "tipo")
        dictionary.updateValue(Date(), forKey: "ultima_alteracao")
        
        
        adicionarCoreData.adicionarDadosCoreData(key: ConstanteInfrastructure().keyAmm, dados: dictionary)
        { result, error in
            print("teste")
            completionHandler(.noError)
        }
    }
}
