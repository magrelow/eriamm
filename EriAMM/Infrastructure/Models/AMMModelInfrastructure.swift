//
//  AMMModelInfrastructure.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 11/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation
import CoreData

struct AMMModelInfrastructure {
    
    var foto: Data?
    var descricao: String?
    var id: String?
    var nome: String?
    var link: String?
    var numero_principal: Int?
    var numero_secundario: Int?
    var tipo: Bool?
    var ultima_alteracao: Date?
    
    init(object: NSManagedObject) {
        self.foto = object.value(forKey:AMMKeysCoreDataModelInfrastructure().foto) as? Data
        self.descricao = object.value(forKey:AMMKeysCoreDataModelInfrastructure().descricao) as? String
        self.id = object.value(forKey:AMMKeysCoreDataModelInfrastructure().id) as? String
        self.nome = object.value(forKey:AMMKeysCoreDataModelInfrastructure().nome) as? String
        self.link = object.value(forKey:AMMKeysCoreDataModelInfrastructure().link) as? String
        self.numero_principal = object.value(forKey:AMMKeysCoreDataModelInfrastructure().numero_principal) as? Int
        self.numero_secundario = object.value(forKey:AMMKeysCoreDataModelInfrastructure().numero_secundario) as? Int
        self.ultima_alteracao = object.value(forKey:AMMKeysCoreDataModelInfrastructure().ultima_alteracao) as? Date
        self.tipo = object.value(forKey:AMMKeysCoreDataModelInfrastructure().tipo) as? Bool
    }
}

struct AMMKeysCoreDataModelInfrastructure {
    let foto = "foto"
    let descricao = "descricao"
    let id = "id"
    let nome = "nome"
    let link = "link"
    let numero_principal = "numero_principal"
    let numero_secundario = "numero_secundario"
    let tipo = "tipo"
    let ultima_alteracao = "ultima_alteracao"
}
