//
//  AdicionarInfrastructureProtocol.swift
//  EriAMM
//
//  Created by Eric Soares Filho on 08/09/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

protocol AdicionarInfrastructureProtocol {
    func adicionarValoresAoCoreData(image: Data, titulo: String, aniMan: Bool, temMan: String, capEpi: String, desc: String, link: String, completionHandler: @escaping(InfraError) -> Void)
}
